
Pod::Spec.new do |spec|
  spec.name         = "FGTrusteer"
  spec.version      = "0.0.3"
  spec.summary      = "A security library for all FG products"
  spec.description  = "This library"
  spec.homepage     = "http://EXAMPLE/FGTrusteer"
  spec.license      = "MIT"
  spec.author             = { "Lahiru Chathuranga" => "lahiruc.freelance@gmail.com" }
  spec.platform     = :ios, "11.0"
  spec.ios.vendored_frameworks = 'FGTrusteer.framework'
  spec.source       = { :http => '' }
  spec.swift_version = '4.0'
end
